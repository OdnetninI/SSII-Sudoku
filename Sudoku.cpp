/* ----------------------- PROBLEMA DE LAS N REINAS ----------------------- */
#include <ga/GASimpleGA.h> // Algoritmo Genetico simple
#include <ga/GA1DArrayGenome.h> //genoma-> array de enteros (dim. 1) alelos
#include <cstring>
#include <iostream>
#include <fstream>
#include <ctime>
float Objective(GAGenome &); // Funcion objetivo --> al final
GABoolean Termina(GAGeneticAlgorithm &);

/*
  From: "PlantillaSudoku.txt" AulaVirtual, material de la práctica
  For: Read and store sudoku data
*/
struct plantilla{
  int tam; // tamaño del sudoku
  int *fijo; // valores fijos iniciales
};

// Lectura del sudoku inicial en S
void leerSudoku (struct plantilla *S, char *nombreF){
  std::ifstream f(nombreF);
  f>>S->tam;
  S->fijo = new int[S->tam*S->tam];

  for(int i=0;i<S->tam*S->tam;i++)
    f>>S->fijo[i];

  f.close();
}


/*
  From: "InicioSudoku.txt" AulaVirtual, material de la práctica
  For:  Initialize GAGenome
*/
// Inicia el genoma sin repetidos en filas y respetando fijos
void InicioSudoku(GAGenome& g){

  //************************************************
  //************************************************
  // Hacer el casting correspondiente para obtener genome
  //************************************************
  //************************************************
  GA1DArrayAlleleGenome<int> & genome = (GA1DArrayAlleleGenome<int> &)g;

  // Obtenemos la estructura auxiliar
  struct plantilla * plantilla1;
  plantilla1 = (struct plantilla *) genome.userData();
  int aux[plantilla1->tam];
  for(int f=0;f<plantilla1->tam;f++){ // por cada fila
  // se inicializa a 0 la estructura auxiliar
    for(int j=0;j<plantilla1->tam;j++) aux[j]=0;
  // se inicializa la fila de 1 a tam sin repetidos
  for(int j=1;j<=plantilla1->tam;j++){
    int v=GARandomInt(0,plantilla1->tam-1);
    while (aux[v]!=0) v=(v+1)%plantilla1->tam;
    aux[v]=j;
  }
    // se colocan los fijos en su lugar
  int i=0;
    while(i<plantilla1->tam){
      while((plantilla1->fijo[(f*plantilla1->tam)+i]==0) && (i<plantilla1->tam)) i++;
      if (i<plantilla1->tam){ // se encuentra un fijo en la plantilla
        // se busca el fijo en aux
        bool encontrado=false;
        for(int j=0;(j<plantilla1->tam) && (!encontrado);j++)
          if (aux[j]==plantilla1->fijo[(f*plantilla1->tam)+i]) {
            encontrado=true;
            aux[j]=aux[i];
          }
        // se pone el fijo en su sitio
        aux[i]=plantilla1->fijo[(f*plantilla1->tam)+i];
      }
      i++;
    }
    // se copia la fila en el genoma
    for(int c=0;c<plantilla1->tam;c++)
      genome.gene((f*plantilla1->tam)+c,aux[c]);
  }
}

/*
  From: "CruceSudoku.txt" AulaVirtual, material de la práctica
  For: Cross Genomes
*/
// Cruce por un punto que coincide con final de una fila
int CruceSudoku(const GAGenome& p1,const GAGenome & p2,GAGenome* c1,GAGenome* c2){

  //************************************************
  //************************************************
  // Hacer el casting correspondiente para obtener m y p
  //************************************************
  //************************************************
  GA1DArrayAlleleGenome<int> & m = (GA1DArrayAlleleGenome<int> &)p1;
  GA1DArrayAlleleGenome<int> & p = (GA1DArrayAlleleGenome<int> &)p2;
  struct plantilla * plantilla1 = (struct plantilla *) m.userData();
  int n=0;

  // buscamos un punto de cruce por filas
  int punto1=GARandomInt(0,m.length());
  while ((punto1%plantilla1->tam)!=0) punto1 = (punto1 + 1)%plantilla1->tam;
  int punto2=m.length()-punto1;

  // el hijo 1 hereda la primera parte del padre 1 y la segunda parte del
  // padre 2.
  if (c1){
    //************************************************
    //************************************************
    // Hacer el casting correspondiente para obtener h1
    //************************************************
    //************************************************
    GA1DArrayGenome<int> & h1 = (GA1DArrayGenome<int> &)(*c1);
    h1.copy(m,0,0,punto1);
    h1.copy(p,punto1,punto1,punto2);
    n++;
  }
  // el hijo 2 hereda la primera parte del padre 2 y la segunda parte del
  // padre 1.
  if (c2){
    //************************************************
    //************************************************
    // Hacer el casting correspondiente para obtener h2
    //************************************************
    //************************************************
    GA1DArrayGenome<int> & h2 = (GA1DArrayGenome<int> &)(*c2);
    h2.copy(p,0,0,punto1);
    h2.copy(m,punto1,punto1,punto2);
    n++;
  }
  return n;
}


// Comprueba si una columna del sudoku tiene valores repetidos
// check contiene la cuenta de valores repetidos

bool checkColumna(int col[], int * check, int tam){
     bool repe=false;

     for(int i=0;i<tam;i++) check[i]=0;

     for(int i=0;i<tam;i++)
             check[col[i]-1]++;
     for(int i=0;i<tam;i++) if (check[i]>1) repe=true;

     return repe;
}

/*
  From: "MutacionSudoku.txt", AulaVirtual, material de la prática
  For: Mutate Genome
*/

// Si un gen debe mutar elegimos si lo hace en fila o columna. En fila es intercambio
// en columna es mas heurístico: cambio un valor repetido por uno que no exista en la columna
int MutacionSudoku(GAGenome& g,float pmut){

  //************************************************
  //************************************************
  // Hacer el casting correspondiente para obtener genome
  //************************************************
  //************************************************
  GA1DArrayAlleleGenome<int> & genome = (GA1DArrayAlleleGenome<int> &)g;
  // Obtenemos la estructura auxiliar
  struct plantilla * plantilla1;
  plantilla1 = (struct plantilla *) genome.userData();
  int nmut=0;
  int aux;
  int fil;
  bool fila;

  int caux[plantilla1->tam];
  int *checkC=new int[plantilla1->tam];

  if (pmut<=0.0) return 0;

  for(int f=0; f<plantilla1->tam; f++)
  for(int c=0; c<plantilla1->tam; c++)
  if (plantilla1->fijo[(f*plantilla1->tam)+c]==0){ // si no es fijo
    if (GAFlipCoin(pmut) ){ // si toca mutar
        if (GAFlipCoin(0.5)) fila = true; // cambiamos fila
        else fila = false; // cambiamos columna
        if (!fila){ // mutamos columna y arreglamos fila

          for(int j=0;j<plantilla1->tam;j++) caux[j]=genome.gene((j*plantilla1->tam)+c); // obtenemos la columna
          if (checkColumna(caux,checkC,plantilla1->tam)){ // si hay repetidos en la columna
            int v1 = GARandomInt(0,plantilla1->tam-1); // v1 es valor repetido
            while (checkC[v1]<=1) v1=(v1+1)%plantilla1->tam;
            v1++;
            int v2 = GARandomInt(0,plantilla1->tam-1); // v2 es un valor que no existe
            while (checkC[v2]!=0) v2=(v2+1)%plantilla1->tam;
            v2++;
            //buscamos donde está v1 y se cambia por v2
            bool encontrado = false;
            for(int j=0;j<plantilla1->tam && !encontrado;j++)
            if ((plantilla1->fijo[j*(plantilla1->tam)+c]==0)&&(genome.gene(j*(plantilla1->tam)+c)==v1)){
              encontrado = true;
              genome.gene((j*plantilla1->tam)+c,v2);
              fil = j;  // v1 esta en fil
            }
            //arreglamos la fila fil donde está v1: buscamos v2 y ponemos v1

            int col=(c+1)%plantilla1->tam;
            while(genome.gene((fil*plantilla1->tam)+col)!=v2) col=(col+1)%plantilla1->tam;
            if (plantilla1->fijo[(fil*plantilla1->tam)+col]==0) { // si v2 no es fijo en fil se lleva a cabo la mutación
              nmut++;
              genome.gene((fil*plantilla1->tam)+col,v1);
            }
            else { // si es fijo se deshace la mutacion
              genome.gene((fil*plantilla1->tam)+c,v1);
            }

          } // fin de si hay repetidos

        }
        else{ // mutamos fila: cambiamos f,c por f,v1
          int v1 = (c + 1) %plantilla1->tam;
          while ((plantilla1->fijo[(f*plantilla1->tam)+v1]!=0)) v1=(v1+1)%plantilla1->tam; //busco un no fijo
          aux = genome.gene((f*plantilla1->tam)+c);
          genome.gene((f*plantilla1->tam)+c,genome.gene((f*plantilla1->tam)+v1));
          genome.gene((f*plantilla1->tam)+v1,aux);
          nmut++;
        }
      } // si toca mutar
    } // si no fijo

  return nmut;
}

void imprimirSudoku(struct plantilla& sudoku) {
  for (int i = 0; i < sudoku.tam; i++) {
    if (i %3 == 0 && i != 0) {
      for (int j = 0; j < (sudoku.tam*2) + 3; j++)
        std::cout << std::string({(char)0xE2,(char)0x94,(char)0x84});
      std::cout << std::endl;
    }
    for (int j = 0; j < sudoku.tam; j++) {
      if (j %3 == 0 && j != 0) std::cout << " " << std::string({(char)0xE2,(char)0x94,(char)0x86});
      std::cout << " " << sudoku.fijo[j + (i*9)];
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

int main(int argc, char* argv[]) {

  if (argc != 6) {
    std::cerr << "Invalid arguments\n";
    std::cerr << "Usage: ./" << argv[0] << " SudokuFile PopSize Selector Pcross Pmut\n";
    std::cerr << "\tSelector: 0 = Roulette Selector, 1 = Tournament Selector\n";
    exit(1);
  }

  if (atoi(argv[3]) > 1 || atoi(argv[2]) < 0) {
    std::cerr << "Invalid Selector\n";
    std::cerr << "Selector: 0 = Roulette Selector, 1 = Tournament Selector\n";
  }

  // Declaramos variables para los parametros del GA y las inicializamos
  int popsize = atoi(argv[2]);
  int ngen = 12000;
  float pmut = atof(argv[5]);
  float pcross = atof(argv[4]);

  struct plantilla sudoku;
  leerSudoku(&sudoku, argv[1]);
  // Conjunto enumerado de alelos --> valores posibles de cada gen del genoma
  GAAlleleSet<int> alelos;
  for(int i=1;i<=sudoku.tam;i++) alelos.add(i);
  imprimirSudoku(sudoku);

  // Creamos el GA. Primero creamos el genoma y creamos una poblacion de genomas
  GA1DArrayAlleleGenome<int> genome(sudoku.tam*sudoku.tam, alelos, Objective, &sudoku);
  genome.initializer(InicioSudoku);
  genome.crossover(CruceSudoku);
  genome.mutator(MutacionSudoku);

  GASimpleGA ga(genome);
  ga.minimaxi(-1);
  ga.populationSize(popsize);
  ga.nGenerations(ngen);
  ga.pMutation(pmut);
  ga.pCrossover(pcross);
  GASelectionScheme* selector = nullptr;
  (atoi(argv[3]) == 0)? selector = new GARouletteWheelSelector() : selector = new GATournamentSelector();
  ga.selector(*selector);
  ga.terminator(Termina);
  ga.evolve(1);

  // Ahora imprimimos el mejor individuo que encuentra el GA y su valor fitness
  GA1DArrayAlleleGenome<int> & genome1 = (GA1DArrayAlleleGenome<int> &)ga.statistics().bestIndividual();
  struct plantilla sudokuSol;
  sudokuSol.tam = sudoku.tam;
  sudokuSol.fijo = new int[sudoku.tam*sudoku.tam];
  for (int i = 0; i < sudoku.tam*sudoku.tam; i++)
    sudokuSol.fijo[i] = genome1.gene(i);

  imprimirSudoku(sudokuSol);
  std::cout << "Mejor valor fitness es " << ga.statistics().minEver() << "\n";
  std::cout << "Generación: " << ga.statistics().generation() << "\n";
  return 0;
}

// Funcion objetivo
float Objective(GAGenome& g) {
  GA1DArrayAlleleGenome<int> & genome = (GA1DArrayAlleleGenome<int> &)g;
  bool numRepe[10];
  bool numRepe2[10];
  int repeticiones = 0;

  for (int j = 0; j < 9; j++) {
    std::memset(numRepe, 0, sizeof(numRepe));
    std::memset(numRepe2, 0, sizeof(numRepe2));
    for (int i = 0; i < 9; i++) {
      int ge = genome.gene(i+(j*9));
      if (numRepe[ge] == false) numRepe[ge] = true;
      else repeticiones+=1;
      ge = genome.gene((i*9)+(j));
      if (numRepe2[ge] == false) numRepe2[ge] = true;
      else repeticiones+=1;
    }
  }

  for (int i = 0; i < 9; i+=3) {
    for (int j = 0; j < 9; j+=3) {
      std::memset(numRepe, 0, sizeof(numRepe));
      for (int p = 0; p < 9; p++) {
        int ge = genome.gene(((j+p/3)*9)+(i+p%3));
        if (numRepe[ge] == false) numRepe[ge] = true;
        else repeticiones+=1;
      }
    }
  }
  return repeticiones;
/*
  GA1DArrayAlleleGenome<int> &sudoku = (GA1DArrayAlleleGenome<int> &)g;
  struct plantilla *sudokuAux = (struct plantilla*)sudoku.userData();
  int dimension = sudokuAux->tam;
  int cuadricula[dimension];
  float parejas = 0;
  //parejas en las columnas
  for(int i=0; i<sudoku.length(); i++){
    for(int j=i+dimension; j<sudoku.length(); j+=dimension)
    if(sudoku.gene(i) == sudoku.gene(j)) parejas++;
  }
  //parejas en las filas
  int f=1;
  for(int i=0; i<sudoku.length(); i++){
    for(int j=i+1; j<f*dimension; j++)
      if(sudoku.gene(i) == sudoku.gene(j)) parejas++;
    if((i+1)%dimension==0) f++;
  }
  //parejas en la misma cuadrículas
  for(int i=0; i<sudoku.length();){
    int var = i;
    //obtenemos un array con los valores que nos interesan
    for(int j=0; j<dimension; j++){
      cuadricula[j] = sudoku.gene(var);
      (j+1)%3 == 0? var+=dimension-3+1 : var++;
    }
    //buscamos parejas en el array que simula a la cuadricula
    for(int j=0; j<dimension; j++)
      for(int k=j+1; k<dimension; k++)
        if(cuadricula[j]==cuadricula[k]) parejas++;
    (i+3)%dimension == 0 ? i+=(dimension-3+1)*3 : i+=3;
  }
  return parejas;*/
}

  // Funcion de terminacion
  // Funcion de terminacion
GABoolean Termina(GAGeneticAlgorithm & ga){
  if ( ga.statistics().minEver()==0 || ga.statistics().generation() == 12000) return gaTrue;
  else return gaFalse;
}
