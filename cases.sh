files="Sudoku-A1.txt Sudoku-A2.txt Sudoku-A3.txt Sudoku-A4.txt Sudoku-A5.txt"
popSize="100 150"
selector="0 1"
pcross="0.8 0.85 0.9 0.95"
pmut="0.01 0.05 0.1 0.125 0.15"

selectorOut="Ruleta Torneo"
sout=""

outputFile="Salida"
csvfile="csv.csv"

rm "$outputFile" "$csvfile"

c=0
total=0

for i in $files
do
  for j in $popSize
  do
    for k in $selector
    do
      for l in $pcross
      do
        for m in $pmut
        do
          total="$(( $total + 1 ))"
        done
      done
    done
  done
done

totalLength=${#total}

echo "Archivo,Poblacion,Selector,Cruce,Mutacion,Fitness,Tiempo(ms),Generacion" > "$csvfile"
for i in $files
do
  for j in $popSize
  do
    for k in $selector
    do
      tempK=$(( $k + 1 ))
      sout=$(echo "$selectorOut" | cut -d ' ' -f $tempK)
      for l in $pcross
      do
        for m in $pmut
        do
          echo "--""Archivo: $i Poblacion: $j Selector $sout Cruce: $l Mutacion: $m""--" >> "$outputFile"
          c="$(( $c + 1 ))"
          printf -v c_ "%0""$totalLength""d" $c
          echo -n "$c_ / $total : "
          echo "Sudoku ""$i" "$j" "$k" "$l" "$m" | tee -a "$outputFile"
          { time ./Sudoku "$i" "$j" "$k" "$l" "$m" >> "$outputFile" ; } 2>> "$outputFile"
          echo >> "$outputFile"
          echo >> "$outputFile"

          secs=$(cat "$outputFile" | tail -n 5 | head -n 1 | cut -f 2 | cut -d 'm' -f 2 | cut -d 's' -f 1 | tr -d '.' | sed 's/^0*//')
          mins=$(cat "$outputFile" | tail -n 5 | head -n 1 | cut -f 2 | cut -d 'm' -f 1 )
          t=$(( $mins * 60 * 1000 ))
          t=$(( $t + $secs ))
          fitnes=$(cat "$outputFile" | tail -n 8 | head -n 1 | cut -d ' ' -f 5)
          gen=$(cat "$outputFile" | tail -n 7 | head -n 1 | cut -d ' ' -f 2)

          echo "$i,$j,$sout,$l,$m,$fitnes,$t,$gen" >> "$csvfile"
        done
      done
    done
  done
done
